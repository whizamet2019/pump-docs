Set ``NODE_ENV`` for better performance
===============================================

The ``NODE_ENV`` environment variable determines the environment
pump.io is running in. This should be set to ``production`` in
production environments or performance will be `significantly`
degraded.

In development environments it should be set to ``development``, which
is the default.

How to set this environment variable will depend on how you start the
pump.io daemon. If you `use the systemd unit shipped with the package
<../administration/upstream-systemd-unit.html>`_, ``NODE_ENV`` will be
set to ``production`` for you automatically.

If you're `not` using the systemd unit, be sure to restart pump.io
after you set up ``NODE_ENV``.
