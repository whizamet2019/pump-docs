Configuration via JSON configuration files
==========================================

.. NOTE:: This is the recommended configuration method for
          non-containerized (standalone) pump.io daemons.

pump.io can use a JSON file for configuration. ``pump`` will look for
configuration files at ``/etc/pump.io.json`` and
``~/.pump.io.json``. These files are expected to have an object at the
top level which has key-value pairs for each configuration option. The
|pump.io.json.sample|_ file should give you an idea of how to use it.

..
    https://stackoverflow.com/a/4836544/1198896
    This is a travesty.

.. |pump.io.json.sample| replace:: ``pump.io.json.sample``
.. _pump.io.json.sample: https://github.com/pump-io/pump.io/blob/master/pump.io.json.sample

You can also override the config file location with the ``-c`` option:

::

    pump -c <CONFIG_FILE>

Configuration files are overridden by CLI flags and environment
variables.

For a list of all available configuration values, see `the reference
list <reference.html>`_.
