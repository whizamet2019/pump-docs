Upgrading to pump.io 2.x from 1.x
==================================

These are the instructions for upgrading pump.io 1.x to 2.x. They will
work for any release in the 1.x series and can be used to upgrade to
any release in the 2.x series, including beta releases.

.. NOTE:: If you're on pump.io 0.3.0 or below, it's recommended to
          upgrade to pump.io 1.x before attempting to upgrade to
          2.x. It's not strictly necessary, but it makes things easier
          if things go wrong since there's less changes that could be
          causing problems.

If at any point you run into trouble, contact the `community
<https://github.com/e14n/pump.io/wiki/Community>`_ and they'll be
happy to sort you out.

For npm-based installs
----------------------

First, check if you have any plugins configured. If you haven't
defined a ``plugins`` key in your ``pump.io.json``, then you don't
have any plugins installed. If you `do` have plugins installed, see
"Migrating plugins" below.

To run the upgrade, invoke:

::

   $ sudo npm install -g pump.io@2

.. NOTE:: If you're trying to upgrade to the latest beta, specify
          ``pump.io@beta`` instead.

Complete your upgrade by restarting your pump.io process.

For source-based installs
-------------------------

First, check if you have any plugins configured. If you haven't
defined a ``plugins`` key in your ``pump.io.json``, then you don't
have any plugins installed. If you `do` have plugins installed, see
"Migrating plugins" below.

Next, fetch new changes, prefixing ``sudo`` as necessary:

::

   $ cd /path/to/your/installation/directory
   $ git fetch

If you've modified templates, you need to save the changes you've
made. You can check to see whether you've made changes by running
``git status`` and ``git log`` - if you have, save them to a file.

Now, discard the changes you've just saved:

::

   $ git checkout .

At this point, ``git status`` should report a working directory with
no modified files.

Now that we're prepared, we'll perform the upgrade itself by checking
out the relevant tag. You'll have to check what the latest version in
the 2.x series is using ``git tag`` - in this case, we're assuming
that 2.0.5 is the latest available.

::

   $ git checkout v2.0.5
   $ npm install

At this point, if you had previously saved template modifications, you
should now use the information you saved to separate files to restore
your modifications. Because Jade's syntax differs significantly from
utml's you won't be able to directly apply the saved changes; however,
you should have saved enough information to reintroduce the same
semantic changes.

If you just restored template changes, it's recommended that you run
the linting process to ensure that your changes are high-quality and
consistent with surrounding code:

::

   $ npm run lint:jade

Finally, regardless of whether or not you modified templates, you need
to rebuild client-side template resources which aren't checked into
git:

::

   $ npm run build

Complete your upgrade by restarting your pump.io process.

Migrating plugins
-----------------

pump.io 2.x upgrades the Express web framework to the 3.x series,
which contains breaking changes. This affects you because plugins
essentially get access to internal application details, including the
Express application object. This means that any plugin that relies on
the behavior of the app object is likely to be affected by Express
3.x's breaking changes. See the `Express 3.x change log
<https://github.com/expressjs/express/blob/master/History.md#300--2012-10-23>`_
for details.

Fixing this can be done in any number of ways - which way you choose
will depend on the nature and complexity of the plugin. For simpler
plugins, it may make sense to simply examine the plugin and compare
all uses of ``app`` with the Express change log. Alternately, it might
be easier to simply start the app, try out behaviors that your plugin
affects or creates, and see what crashes.
